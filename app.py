"""Flask API with various endpoints."""
from flask import Flask

app = Flask(__name__)

@app.get('/')
@app.get('/info')
def get_info():
    """return API info."""
    info = {
        "API": "my-api",
        "Version": "0.0.1",
        "Team": "OPS"
    }
    app.logger.debug(f"{info=}")
    return info

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)